import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';

import { CardOffersComponent } from './card-offers';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
	declarations: [
    CardOffersComponent
  ],
	imports: [
    IonicModule,
    IonicImageLoader
  ],
	exports: [
    CardOffersComponent
  ]
})
export class CardOffersModule {}

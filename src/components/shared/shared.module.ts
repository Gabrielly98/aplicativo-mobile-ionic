import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FlashComponent } from './flash/flash';
import { TimeBarComponent } from './flash/time-bar/time-bar';

export const components = [
  FlashComponent,
  TimeBarComponent
];

@NgModule({
	declarations: [components],
	imports: [
		BrowserModule,
		BrowserAnimationsModule
  ],
  exports: [components]
})
export class SharedModule {}

import { Injectable} from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';

import { AppConfig } from '../providers/app.config';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private appConfig: AppConfig,
    private localStorageProvider: LocalStorageProvider
  ) {}

  intercept (request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const requestUrl: Array<any> = request.url.split('/');
    const apiUrl: Array<any> = this.appConfig.apiEndpoint.split('/');

    return this.getToken('token')
      .mergeMap((token: any) => {

        if (token && (requestUrl[2] === apiUrl[2])) {
          request = request.clone({ setHeaders: { Authorization: `Bearer ${token}` }});
          return next.handle(request);
        } else {
          return next.handle(request);
        }

      });
  }

  private getToken(key: string): Observable<any> {
    return Observable.fromPromise(this.localStorageProvider.get(key));
  }
};

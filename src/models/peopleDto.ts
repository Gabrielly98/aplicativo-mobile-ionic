export class PeopleDto {
  private _active: Boolean;
  private _name_reason: String;
  private _name_fantasy: String;
  private _cpf_cnpj: Number;
  private _rg_uf: String;
  private _rg_ie: Number;
  private _type_people: String;
  private _sex: String;

  constructor() {}

  /**
   * Getter active
   * @return {Boolean}
   */
	public get active(): Boolean {
		return this._active;
	}

  /**
   * Getter name_reason
   * @return {String}
   */
	public get name_reason(): String {
		return this._name_reason;
	}

  /**
   * Getter name_fantasy
   * @return {String}
   */
	public get name_fantasy(): String {
		return this._name_fantasy;
	}

  /**
   * Getter cpf_cnpj
   * @return {Number}
   */
	public get cpf_cnpj(): Number {
		return this._cpf_cnpj;
	}

  /**
   * Getter rg_uf
   * @return {String}
   */
	public get rg_uf(): String {
		return this._rg_uf;
	}

  /**
   * Getter rg_ie
   * @return {Number}
   */
	public get rg_ie(): Number {
		return this._rg_ie;
	}

  /**
   * Getter type_people
   * @return {String}
   */
	public get type_people(): String {
		return this._type_people;
	}

  /**
   * Getter sex
   * @return {String}
   */
	public get sex(): String {
		return this._sex;
	}

  /**
   * Setter active
   * @param {Boolean} value
   */
	public set active(value: Boolean) {
		this._active = value;
	}

  /**
   * Setter name_reason
   * @param {String} value
   */
	public set name_reason(value: String) {
		this._name_reason = value;
	}

  /**
   * Setter name_fantasy
   * @param {String} value
   */
	public set name_fantasy(value: String) {
		this._name_fantasy = value;
	}

  /**
   * Setter cpf_cnpj
   * @param {Number} value
   */
	public set cpf_cnpj(value: Number) {
		this._cpf_cnpj = value;
	}

  /**
   * Setter rg_uf
   * @param {String} value
   */
	public set rg_uf(value: String) {
		this._rg_uf = value;
	}

  /**
   * Setter rg_ie
   * @param {Number} value
   */
	public set rg_ie(value: Number) {
		this._rg_ie = value;
	}

  /**
   * Setter type_people
   * @param {String} value
   */
	public set type_people(value: String) {
		this._type_people = value;
	}

  /**
   * Setter sex
   * @param {String} value
   */
	public set sex(value: String) {
		this._sex = value;
	}

}

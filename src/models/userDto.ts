export class UserDto {
  private _id: Number;
  private _email: String;
  private _password: String;
  private _person_id: Number;
  private _status: String;

  constructor() {}

  /**
   * Getter id
   * @return {Number}
   */
	public get id(): Number {
		return this._id;
	}

  /**
   * Getter email
   * @return {String}
   */
	public get email(): String {
		return this._email;
	}

  /**
   * Getter password
   * @return {String}
   */
	public get password(): String {
		return this._password;
	}

  /**
   * Getter person_id
   * @return {Number}
   */
	public get person_id(): Number {
		return this._person_id;
	}

  /**
   * Getter status
   * @return {String}
   */
	public get status(): String {
		return this._status;
	}

  /**
   * Setter id
   * @param {Number} value
   */
	public set id(value: Number) {
		this._id = value;
	}

  /**
   * Setter email
   * @param {String} value
   */
	public set email(value: String) {
		this._email = value;
	}

  /**
   * Setter password
   * @param {String} value
   */
	public set password(value: String) {
		this._password = value;
	}

  /**
   * Setter person_id
   * @param {Number} value
   */
	public set person_id(value: Number) {
		this._person_id = value;
	}

  /**
   * Setter status
   * @param {String} value
   */
	public set status(value: String) {
		this._status = value;
	}

}

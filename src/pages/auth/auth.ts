import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';

import { AuthProvider } from '../../providers/auth/auth';
import { FlashProvider } from '../../providers/shared/flash/flash';

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

  protected form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public flashProvider: FlashProvider,
    public _auth: AuthProvider,
  ) {
    this.form = new FormGroup({
      email_cpf: new FormControl(null, Validators.required)
    });
  }

  protected submit(): void {
    if (this.form.invalid) {
      return this.flashProvider.show('Formulário inválido. Verifique os dados e tente novamente!', 'error');
    }

    let loader = this.loadingCtrl.create({});
    loader.present();

    let param: any;
    let email_cpfcnpj = this.form.get('email_cpf').value;

    if (Number(email_cpfcnpj)) {
      param = {cpf_cnpj: Number(email_cpfcnpj)};
    } else {
      param = {email: email_cpfcnpj};
    }

    this._auth.getName(param).then((res) => {
      loader.dismiss();
      this.navCtrl.push('LoginPage', {name: res['data'], params: param});

    }, (err) => {
      loader.dismiss();

      if (err.status == 404) {
        return this.navCtrl.push('RegisterPage', {params: param});
      } else {
        this.flashProvider.show(`${err.status + ' -'} Não conseguimos processar sua solicitação!`, 'error', 10000);
      }
    });
  }
}

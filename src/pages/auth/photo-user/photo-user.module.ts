import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';

import { PhotoUserPage } from './photo-user';
import { PipesModule } from '../../../pipes/pipes.module';

@NgModule({
  declarations: [
    PhotoUserPage,
  ],
  imports: [
    IonicPageModule.forChild(PhotoUserPage),
    PipesModule
  ],
  providers: [
    Camera
  ]
})
export class PhotoUserPageModule {}

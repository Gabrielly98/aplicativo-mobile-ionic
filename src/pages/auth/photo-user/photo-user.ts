import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';

@IonicPage()
@Component({
  selector: 'page-photo-user',
  templateUrl: 'photo-user.html',
})
export class PhotoUserPage {

  params: any;
  photo: any = 'assets/imgs/photo-user.png';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public camera: Camera
  ) {
  }

  ionViewDidLoad() {
    this.navCtrl.remove(0, this.viewCtrl.index);
    this.navCtrl.insert(0, 'TabsPage')

    this.params = this.navParams.get('data');
  }

  takePicture() {
    let cameraOptions : CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      allowEdit: true
    };

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.photo = imageData;
    }, (err)=> {
      console.log(err);
      //this.alerta(err.toString(), "Atenção");
    });
  }

  photoGallery() {
    let cameraOptions : CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 500,
      targetHeight: 500,
      correctOrientation: true,
      saveToPhotoAlbum: true,
      allowEdit: true
    }

    this.camera.getPicture(cameraOptions).then((imageData) => {
      this.photo = imageData;
    }, (err)=> {
      console.log(err);
    });
  }

  submitPhoto() {
    console.log(this.photo);
  }

  openPage(page) {
    this.navCtrl.setRoot(page);
  }
}

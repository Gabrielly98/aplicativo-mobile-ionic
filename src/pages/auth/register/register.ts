import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events } from 'ionic-angular';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import lodash from 'lodash';

import { FlashProvider } from '../../../providers/shared/flash/flash';
import { AuthProvider } from '../../../providers/auth/auth';
import { matchOtherValidator } from '../../../providers/shared/match-other-validator/match-other-validator';
import { ValidadorCpfCNPJ } from '../../../providers/shared/validator-cpf-cnpj/validador';
import { LocalStorageProvider } from '../../../providers/local-storage/local-storage';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  form: FormGroup;
  user: FormGroup;
  partner: FormGroup;
  params: any;
  fisica: Boolean = true;

  constructor(
    public fb: FormBuilder,
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public flashProvider: FlashProvider,
    public ValidadorCpfCNPJ: ValidadorCpfCNPJ,
    private localStorage: LocalStorageProvider,
    private events: Events,
    public _auth: AuthProvider,
  ) {
    let emailRegex = /^[a-z0-9._+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0–9]{2,4}$/;

    this.form = fb.group({
      user: this.user = fb.group({
        email: [null, [Validators.required, Validators.pattern(emailRegex)]],
			  password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
        password_confirmation: ['', [matchOtherValidator('password')]]
      }),
      partner: this.partner = fb.group({
        type_person: ['Física'],
        name_reason: [null, [Validators.required, Validators.minLength(12)]],
        cpf_cnpj: [null, [Validators.required, Validators.minLength(14), Validators.maxLength(18)]],
      })
    })
  }

  ionViewDidLoad(): void {
    this.params = this.navParams.get('params');

    if (this.params != undefined && this.params.cpfcnpj != undefined) {
      this.partner.patchValue({cpf_cnpj: this.params.cpfcnpj})
    } else if (this.params != undefined && this.params.email != undefined) {
      this.user.patchValue({email: this.params.email})
    }
  }

  protected typepartnerSelect(selected): void {
    selected == "Física" ? this.fisica = true : this.fisica = false;
  }

  async valida(formControlName: string, value: string, type: string) {
    const result = await this.ValidadorCpfCNPJ.valida(value, type);

    if (result == false)
      this.partner.get(formControlName).setErrors({ cpfCnpjValido: !result });
  }

  protected submit(): void {
    if (this.form.invalid) {
      return this.flashProvider.show('Formulário inválido. Verifique os dados e tente novamente!', 'error');
    }

    let loader = this.loadingCtrl.create({});
    loader.present();

    let form = lodash.cloneDeep(this.form.value);
    form.partner.cpf_cnpj = form.partner.cpf_cnpj.toString().replace(/[^\d]+/g, '');

    this._auth.register(form).then(async res => {
      this.flashProvider.show('Cadastro realizado com sucesso!', 'success', 5000);
      const dataLogin = res;

      // Salva token LocalStorage
      this.localStorage.set('token', dataLogin['data']['auth']['token']);

      // Salva refreshToken LocalStorage
      this.localStorage.set('refreshToken', dataLogin['data']['auth']['refreshToken']);

      // Salva dados do usuário
      this.localStorage.set('user', res['data']['user']);
      this.events.publish('user:logged', res['data']['user']); // Atualiza Sidemenu
      this.navCtrl.setRoot('TabsPage', { params: res['data']['user'] });
      // this.navCtrl.push('PhotoUserPage', { data: res['data']['user'] });
      loader.dismiss();
    }, (err) => {
      loader.dismiss();
      if (err.status === 422 && Array.isArray(err.error.message)) {
        this.setErrorsForm(err.error.message);
        return;
      }

      this.flashProvider.show(`${err.status + ' - '} Não conseguimos processar sua solicitação!`, 'error', 10000);
    });
  }

  private setErrorsForm(errors) {
    for (let e of errors) {
      if (e.field == 'email') {
        this.user.get(e.field).setErrors({ duplicity: true });
      } else if (e.field == 'cpf_cnpj') {
        this.partner.get(e.field).setErrors({ duplicity: true });
      }
    }
  }

}

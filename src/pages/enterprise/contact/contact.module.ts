import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactPage } from './contact';
import { GoogleMapsProvider } from '../../../providers/google-maps/google-maps';

@NgModule({
  declarations: [
    ContactPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactPage),
  ],
  providers: [
    GoogleMapsProvider
  ]
})
export class ContactPageModule {}

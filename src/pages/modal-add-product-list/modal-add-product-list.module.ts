import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IonicImageLoader } from 'ionic-image-loader';
import { ModalAddProductListPage } from './modal-add-product-list';

@NgModule({
  declarations: [
    ModalAddProductListPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalAddProductListPage),
    IonicImageLoader
  ],
  exports: [
    ModalAddProductListPage
  ]
})

export class ModalAddProductListModule { }

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { CardOffersModule } from '../../../../../components/card-offers/card-offers.module';
import { LongPressModule } from 'ionic-long-press';
import { IonicImageLoader } from 'ionic-image-loader';
import { DetailsShoppingListPage } from './details-shopping-list';
import { SocialSharing } from '@ionic-native/social-sharing';

@NgModule({
  declarations: [
    DetailsShoppingListPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailsShoppingListPage),
    CardOffersModule,
    LongPressModule,
    IonicImageLoader
  ],
  providers: [
    BarcodeScanner,
    SocialSharing
  ]
})
export class DetailsShoppingListPageModule {}

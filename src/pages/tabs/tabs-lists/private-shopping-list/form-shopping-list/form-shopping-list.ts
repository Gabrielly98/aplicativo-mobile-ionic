import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController, ViewController } from 'ionic-angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as moment from 'moment';

import { ShoppingListProvider } from '../../../../../providers/shopping-list/shopping-list';
import { Toast } from '@ionic-native/toast';
import { FlashProvider } from '../../../../../providers/shared/flash/flash';

@IonicPage()
@Component({
  selector: 'page-form-shopping-list',
  templateUrl: 'form-shopping-list.html',
})
export class FormShoppingListPage {

  loading: any;
  form: FormGroup;
  now: string;

  dataList: any;
  title: string;
  titleButton: string;

  constructor(
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public toast: Toast,
    public flashProvider: FlashProvider,
    public loadingCtrl: LoadingController,
    public _shoppingListProvider: ShoppingListProvider
  ) {
    // Define o idioma do moment
    moment.locale('pt-BR');
    this.now = moment().format();

    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
      note: new FormControl(null, [Validators.maxLength(255)]),
      date_for_purchase: new FormControl(null),
      public: new FormControl(false),
    });

    this.dataList = this.navParams.get('dataList');
    if (this.dataList) {
      this.title = "Atualizar lista";
      this.titleButton = "Atualizar";
    } else {
      this.title = "Nova lista";
      this.titleButton = "Cadastrar";
    }
  }

  ionViewDidLoad() {
    if (this.dataList) {
      this.form.patchValue({
        name: this.dataList.name,
        note: this.dataList.note,
        date_for_purchase: this.dataList.date_for_purchase,
        public: this.dataList.public,
      });
    }
  }

  submit() {
    this.showLoader();

    let data = this.form.value;
    if (data.date_for_purchase) {
      data['date_for_purchase'] = moment(data.date_for_purchase).format("YYYY-MM-DD HH:mm:ss");
    }

    if (this.dataList) {
      this._shoppingListProvider.update(this.dataList.id, this.form.value)
        .then((res) => {
          this.loading.dismiss();
          this.showToast("Lista de compra atualizada com sucesso");
          this.viewCtrl.dismiss({ code: 'success' });
        }, (err) => {
          console.log('erro ', err);

          this.loading.dismiss();
          this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        });
    } else {
      this._shoppingListProvider.post(this.form.value)
        .then((res) => {
          this.loading.dismiss();
          this.showToast("Lista de compra cadastrada com sucesso");
          this.viewCtrl.dismiss({ code: 'success' });
        }, (err) => {
          console.log('erro ', err);

          this.loading.dismiss();
          this.flashProvider.show('Erro interno. Tente novamente!', 'error', 3000)
        });
    }
  }

  clearDateTime() {
    this.form.get('date_for_purchase').setValue(null);
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  back() {
    this.viewCtrl.dismiss();
  }
}

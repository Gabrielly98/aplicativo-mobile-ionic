import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController } from 'ionic-angular';
import { Toast } from '@ionic-native/toast';
import lodash from 'lodash';
import { FlashProvider } from '../../../../providers/shared/flash/flash';
import { LocalStorageProvider } from '../../../../providers/local-storage/local-storage';

import { ShoppingListProvider } from '../../../../providers/shopping-list/shopping-list';


@IonicPage()
@Component({
  selector: 'page-public-shopping-list',
  templateUrl: 'public-shopping-list.html',
})
export class PublicShoppingListPage {

  loading: any;

  lists: any[] = [];
  listsUserLogged: any[] = [];

  page = 1;
  perPage = 0;
  total = 0;
  totalPage = 0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public toast: Toast,
    public flashProvider: FlashProvider,
    public localStorageProvider: LocalStorageProvider,
    public _shoppingListProvider: ShoppingListProvider
  ) {
  }

  ionViewDidEnter() {
    this.getLists(true);
  }

  async getLists(clearList = false, page?: number, wBy?: string, w?: string) {
    this.showLoader();

    await this._shoppingListProvider.getAllPublic(true, true, page, wBy, w)
      .then(async res => {
        this.total = res['data']['total'];
        this.perPage = res['data']['perPage'];
        this.page = res['data']['page'];

        if (clearList) {
          this.lists = [];
        }

        let lists = []
        res['data']['data'].forEach(element => {
          lists.push(element)
        });

        await this.separatesUserLists(lists);

        this.localStorageProvider.set('listsPublic', { listsUser: this.listsUserLogged, lists: this.lists });
        this.loading.dismiss();
      }, (err) => {
        console.log('erro ', err);
        
        this.loading.dismiss();
        if (err.status !== 404) {
          this.flashProvider.show('Não foi possível buscar as listas no servidor!', 'error', 3000)
        }

        this.localStorageProvider.get('listsPublic').then(res => {
          this.lists = res && res.lists ? res.lists : [];
          this.listsUserLogged = res && res.listsUser ? res.listsUser : [];
        });
      });
  }

  async separatesUserLists(lists) {
    await this.localStorageProvider.get('user').then(user => {
      this.listsUserLogged = lodash.filter(lists, { user_id: user.id });
      this.lists = lodash.difference(lists, this.listsUserLogged);
    }, err => {
      this.lists = lists;
    });
  }

  showToast(message) {
    this.toast.show(message, '5000', 'bottom').subscribe(toast => { });
  }

  goDetails(list, listPublic: boolean) {
    if (listPublic) {
      this.navCtrl.push('PublicDetailsShoppingListPage', { data: list })
    } else {
      this.navCtrl.push('DetailsShoppingListPage', { data: list })
    }
  }

  showLoader() {
    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...'
    });

    this.loading.present();
  }

  doInfinite(infiniteScroll) {
    this.page += 1;

    setTimeout(() => {
      this.getLists(false, this.page);
      infiniteScroll.complete();
    }, 2000);
  }

  async doRefresh(refresher) {
    setTimeout(async () => {
      await this.getLists(true);
      refresher.complete();
    }, 2000);
  }
}

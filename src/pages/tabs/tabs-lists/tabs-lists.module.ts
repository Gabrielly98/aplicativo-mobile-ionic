import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsListsPage } from './tabs-lists';

@NgModule({
  declarations: [
    TabsListsPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsListsPage),
  ]
})
export class TabsListsModule {}

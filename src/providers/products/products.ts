import { Injectable } from '@angular/core';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/retry';

import { ApiService } from '../api';

@Injectable()
export class ProductsProvider {

  constructor(public api: ApiService) {
  }

  public getAll(obj?: { whereBy?: string, where?: string }) {
    return new Promise((resolve, reject) => {
      this.api.get('app/products', {
        wBy: obj && obj.whereBy ? obj.whereBy : '',
        w: obj && obj.where ? obj.where : ''
      })
      .subscribe(res => {
        resolve(res);
      }, err => {
        reject(err);
      });
    });
  }

  public get(idProduct: number) {
    return new Promise((resolve, reject) => {
      this.api.get('app/products/' + idProduct)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }

  public getBySection(idSection: number, page: number = 1, wBy: string = 'id', w: string = '') {
    return new Promise((resolve, reject) => {
      this.api.get(`app/products/section/${idSection}?page=${page}&wBy=${wBy}&w=${w}`)
        .subscribe(res => {
          resolve(res);
        }, err => {
          reject(err);
        });
    });
  }
}

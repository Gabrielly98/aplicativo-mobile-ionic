import { Injectable } from '@angular/core';

// https://www.joshmorony.com/creating-a-flash-message-service-in-ionic/

@Injectable()
export class FlashProvider {
  constructor() {}
  
  /**
   * 
   * @param message Mensagem a ser exibida
   * @param type Estilo do toast: success, info, warning, error, dark, light, secondary, primary
   * @param duration Tempo de duração do Toast
   */
  show(message: string, type?: string, duration?: number, ) {}

  /**
   * Oculta o toast
   */
  hide() {}

}
